#!/usr/bin/env bash

# drop the database
# this step relies on having a WP CLI installed and configured but one could
# use any other method to get the database

wp @dev db export --add-drop-table dev_database.sql


(cd htdocs/wp-content/themes/llogaterssabadell; yarn build)

(cd htdocs/wp-content/; rm debug.log)

# copy all wp-content content except node modules and cvs related stuff. Please 
# note that some content may fall to copy due to excludes
rsync -aphvP --update --delete --cvs-exclude --exclude node_modules/ ~/code/VVV/www/llogateressabadell/htdocs/wp-content llogateressbd@miqueladell.com:/home/llogateressbd/www

# copy the database 
rsync -aphvP --update --delete ~/code/VVV/www/llogateressabadell/htdocs/dev_database.sql llogateressbd@miqueladell.com:/home/llogateressbd/www/dev_database.sql

wp @production db import dev_database.sql
wp @production search-replace "llogateressabadell.test" "llogateressabadell.cat"