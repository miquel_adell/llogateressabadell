<header class="banner">
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    <div class="description">Reivindiquem els nostres drets.<br>Defensem un lloguer just</div>
  </div>
</header>
<nav class="nav-primary">
  <div class="container">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
  @endif
  </div>
</nav>
