<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
  <div class="footer__description">Sindicat de Llogaters i Llogateres de Sabadell,<br>amb la col·laboració de <a href="http://afectatscrisisabadell.cat">PAH Sabadell</a></div>
</footer>
