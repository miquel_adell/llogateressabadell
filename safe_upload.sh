#!/usr/bin/env bash

# drop the database
# this step relies on having a WP CLI installed and configured but one could
# use any other method to get the database
wp @dev db export --add-drop-table dev_database.sql


(cd htdocs/wp-content/themes/llogaterssabadell; yarn build)

(cd htdocs/wp-content/; rm debug.log)

rsync -aphvP --update --delete --cvs-exclude --exclude node_modules/ ~/code/VVV/www/llogateressabadell/htdocs/wp-content/themes llogateressbd@miqueladell.com:/home/llogateressbd/www/wp-content
