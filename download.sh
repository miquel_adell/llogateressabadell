#!/usr/bin/env bash
wp @production db export --add-drop-table production_database.sql
rsync -aphvP --update --delete llogateressbd@miqueladell.com:/home/llogateressbd/www/production_database.sql ~/code/VVV/www/llogateressabadell/htdocs/production_database.sql 
wp @dev db import production_database.sql
wp @dev search-replace "http://llogateressabadell.cat/" "http://llogateressabadell.test/"

rsync -aphvP --update --delete llogateressbd@miqueladell.com:/home/llogateressbd/www/wp-content/uploads ~/code/VVV/www/llogateressabadell/htdocs/wp-content/
